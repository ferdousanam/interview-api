<?php

namespace App\Filters;


class FindStudentFilter extends FindInvitationFilter
{

    public function gpa($value)
    {
        $this->builder->whereHas( 'educations', function ($qury) use ($value) {
            $qury->whereGpa( $value );
        } );
    }

    public function score($value)
    {
        $this->builder->whereHas( 'educations', function ($qury) use ($value) {
            $qury->wherePercentage( $value );
        } );
    }

    public function program($value)
    {
        $this->builder->whereHas( 'educations', function ($qury) use ($value) {
            $qury->whereTitle( $value );
        } );
    }

    public function grade($value)
    {
        $this->builder->whereHas( 'educations', function ($qury) use ($value) {
            $qury->whereGpa( $value );
        } );
    }
}