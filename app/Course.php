<?php

namespace App;


class Course extends Models
{
    protected $table = 'courses';
    protected $fillable = ['education_level_id', 'user_id', 'title', 'fee', 'starting_date', 'description', 'licence_required', 'licence_id', 'online_course', 'online_url', 'online_site_name', 'online_course_image', 'online_course_video'];

    //Save in MongoDB
    protected static function boot() {
        parent::boot();
        static::created( function () {
            $data = \App\User::mongoSave();
        });

        static::updated( function () {
            $data = \App\User::mongoSave();
        });

        static::deleted( function () {
            $data = \App\User::mongoSave();
        });
    }

    public function educationLevel()
    {
        return $this->belongsTo( EducationLevel::class );
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function licence()
    {
        return $this->belongsTo( Licence::class );
    }

    public function serviceModes()
    {
        return $this->hasMany( CourseServiceMode::class );
    }
}
