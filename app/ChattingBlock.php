<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChattingBlock extends Model
{
    protected $table = 'chatting_blocks';
    protected $fillable = ['from', 'to', 'status'];
}
