<?php

namespace App;


class SocialTopic extends Models
{
    protected $fillable = ['user_id', 'search_query', 'queue_status'];
}
