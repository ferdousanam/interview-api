<?php

namespace App\Http\Controllers;

use App\EducationLevel;
use App\Program;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    private $username;
    private $institute_id;

    public function __construct()
    {
        $this->username = 'irvine-high-school';
        $this->institute_id = 28;
    }

    public function index()
    {
        $data = User::with(['institute'])->whereUserName($this->username)->first();
        if ($data->count() > 0) {
            return response()->json(['success' => true, 'data' => $data, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'data' => 'Whoops! Data Not Found', 'status' => 401], 200);
        }
    }

    public function getMedias()
    {
        $data = User::with(['videos' => function ($q) {
            $q->select(['mediable_id', 'id', 'title', 'filename', 'mime_type']);
        }])->whereUserName($this->username)->first();
        $videos = $data->videos
            ->transform(function ($part) {
                $part->makeHidden('mediable_id');
                return $part;
            });
        if ($data->count() > 0) {
            return response()->json(['success' => true, 'data' => $videos, 'server_url' => url("storage/medias"), 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'data' => 'Whoops! Data Not Found', 'status' => 401], 200);
        }
    }

    public function getTopDisciplines()
    {
        $sql = null;
        DB::enableQueryLog();
        $subQuery = DB::table('programs')
            ->selectRaw('SUM(JSON_LENGTH(programs.degrees)) as total_count')
            ->from('programs')
            ->where('institute_id', $this->institute_id)
            ->limit(1)->first();
        $data = DB::table('programs')
            ->select('programs.institute_id', 'programs.title', 'programs.degrees')
            ->selectRaw('JSON_LENGTH(programs.degrees) as count_degrees')
            /*->addSelect(['total_count' => function ($q) {
                $q->selectRaw('SUM(JSON_LENGTH(programs.degrees))')
                    ->from('programs')
                    ->whereColumn('institute_id', 'programs.institute_id')
                    ->limit(1);
            }])*/
            ->selectRaw('JSON_LENGTH(`programs`.`degrees`) / (select SUM(JSON_LENGTH(`programs`.`degrees`)) from `programs` where `institute_id` = `programs`.`institute_id` limit 1) * 100 as percent')
            /*->selectRaw('(JSON_LENGTH(programs.degrees) /' . $subQuery->total_count . ') as percentage')*/
            ->groupBy('programs.title')
            ->orderBy('count_degrees', 'DESC')
            ->limit(30)->get();
        $sql = DB::getQueryLog();
        if ($data->count() > 0) {
            return response()->json(['success' => true, 'total' => $subQuery->total_count, 'data' => $data, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'total' => 0, 'data' => 'Whoops! No Data Found.', 'status' => 401], 200);
        }
    }

    public function getAllEducationLevels()
    {
        $data = EducationLevel::all();
        if ($data->count() > 0) {
            return response()->json(['success' => true, 'data' => $data, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'data' => 'Whoops! Data Not Found', 'status' => 401], 200);
        }
    }

    public function getProgramsByEducationLevel(Request $request, $education_level_id)
    {
        if ($request->header('Authorization', false)) {
            if ($request->header('Authorization') !== '123454321') {
                return response()->json(['success' => false, 'data' => 'You are not Authorized!', 'status' => 401], 200);
            }
        } else {
            return response()->json(['success' => false, 'data' => '\'Authorization\' is required', 'status' => 401], 200);
        }

        $programs = Program::where('institute_id', $this->institute_id)
            ->select(['id', 'title', 'fee', 'admission_date', 'application_fee', 'degrees']);
        if ($education_level_id != 'null') {
            $programs->where('education_level_id', $education_level_id);
        }
        $data = $programs->limit(25)->get();
        if ($data->count() > 0) {
            return response()->json(['success' => true, 'data' => $data, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'data' => 'Whoops! Data Not Found', 'status' => 401], 200);
        }
    }

    public function filterPrograms(Request $request)
    {
        if (!isset($request->_mediusware)) {
            return response()->json(['success' => false, 'error' => '\'_mediusware\' method is required', 'status' => 401], 200);
        } elseif ($request->_mediusware !== 'mediusware_method') {
            return response()->json(['success' => false, 'error' => '\'_mediusware\' method is incorrect', 'status' => 401], 200);
        }
        $programs = Program::with('educationLevel')->where('institute_id', $this->institute_id);
        if (isset($request->education_level_id) && $request->education_level_id != 'null') {
            $programs->where('education_level_id', $request->education_level_id);
        }
        if (isset($request->program_id) && $request->education_level_id != 'null') {
            $programs->where('id', $request->program_id);
        }
        if (isset($request->program_title)) {
            $programs->where('title', 'like', '%' . $request->program_title . '%');
        }
        if (isset($request->sort_by)) {
            $sort = $request->sort_by == 0 ? 'ASC' : 'DESC';
            $programs->orderBy('title', $sort);
        } else {
            $programs->orderBy('admission_date', 'DESC');
        }
        $data = $programs->get();
        if ($data->count() > 0) {
            return response()->json(['success' => true, 'data' => $data, 'status' => 200], 200);
        } else {
            return response()->json(['success' => false, 'data' => 'Whoops! Data Not Found', 'status' => 401], 200);
        }
    }
}
