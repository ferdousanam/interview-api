<?php

namespace App;

class Interest extends Models
{
    protected $guarded = [];

    //Save in MongoDB
    protected static function boot() {
        parent::boot();
        static::created( function () {
            $data = \App\User::mongoSave();
        });

        static::updated( function () {
            $data = \App\User::mongoSave();
        });

        static::deleted( function () {
            $data = \App\User::mongoSave();
        });
    }

    public function users()
    {
        return $this->belongsTo( User::class );
    }
}
