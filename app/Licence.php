<?php

namespace App;


class Licence extends Models
{
    protected $table = 'licences';

    protected $fillable = ['user_id', 'valid_till', 'title', 'start_date', 'authority'];

    //Save in MongoDB
    protected static function boot() {
        parent::boot();
        static::created( function () {
            $data = \App\User::mongoSave();
        });

        static::updated( function () {
            $data = \App\User::mongoSave();
        });

        static::deleted( function () {
            $data = \App\User::mongoSave();
        });
    }


    public function user()
    {
        return $this->belongsTo( User::class );
    }

    public function courses()
    {
        return $this->hasMany( Course::class );
    }
}
