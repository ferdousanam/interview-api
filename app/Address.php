<?php

namespace App;


class Address extends Models
{
    protected $table = 'addresses';
    protected $fillable = ['country_id', 'state_id', 'city', 'address', 'lat', 'lng', 'addressable_id', 'addressable_type'];

    protected $appends = ['country','state'];

    //Save in MongoDB
    protected static function boot() {
        parent::boot();
        static::created( function () {
            $data = \App\User::mongoSave();
        });

        static::updated( function () {
            $data = \App\User::mongoSave();
        });

        static::deleted( function () {
            $data = \App\User::mongoSave();
        });
    }

    public function country()
    {
        return $this->belongsTo( Country::class );
    }

    public function state()
    {
        return $this->belongsTo( State::class );
    }

    public function addressable()
    {
        return $this->morphTo();
    }

    public function getCountryAttribute()
    {
        return $this->country()
            ->whereId($this->country_id)
            ->select( ['id', 'title'] )->first();
    }
    public function getStateAttribute()
    {
        return $this->state()
            ->whereId($this->state_id)
            ->select( ['id', 'title'] )->first();
    }
}
