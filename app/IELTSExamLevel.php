<?php

namespace App;


class IELTSExamLevel extends Models
{
    protected $table = 'ielts_exam_levels';

    protected $fillable = ['title', 'status', 'created_by', 'approved_by', 'approved_at'];

    public function ielts(){
        return $this->hasMany(Ielts::class);
    }
}
