<?php

namespace App;

class Subscribe extends Models
{
  protected $fillable = ['email', 'subscribe'];
}
