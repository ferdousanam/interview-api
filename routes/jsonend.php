<?php

Route::group(['prefix' => 'json'], function () {
    /**
     * Profile page
     */
    Route::get('profile', 'ProfileController@index');
    Route::get('profile/medias', 'ProfileController@getMedias');
    Route::get('profile/top-disciplines', 'ProfileController@getTopDisciplines');
    Route::get('profile/education-levels', 'ProfileController@getAllEducationLevels');
    Route::post('profile/programs/filter', 'ProfileController@filterPrograms');
    Route::get('profile/programs/{education_level_id}', 'ProfileController@getProgramsByEducationLevel');
} );


