<?php
/**
 * Created by PhpStorm.
 * User: alamincse
 * Date: 11/9/2018
 * Time: 11:23 PM
 */

return [
    'video'=>[
        'video/avi',
        'video/mp4',
        'video/mpeg',
        'video/quicktime',
        'image/jpeg',
        'image/png',
    ],
    'image'=>[
        'image/jpeg',
        'image/jpg',
        'image/png',
    ]
];
